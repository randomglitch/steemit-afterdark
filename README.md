
<img src="https://gitlab.com/randomglitch/steemit-afterdark/raw/f697a0af4156d3b3fedc1e2cb4ab58ac9e2e1cdc/steemit-screen.png" alt="screenshot" />

## Installation
Use the Stylish extension for the browser you prefer.  
 * Get the [Firefox][1] addon.
 * Get the [Chrome][2] extension.

## Notes
If you have any commments or suggestions contact me at [randomglitch@gmail.com][3].

[1]: https://addons.mozilla.org/en-US/firefox/addon/stylish/ "Stylish for Firefox"
[2]: https://chrome.google.com/webstore/detail/stylish/fjnbnpbmkenffdnngjfgmeleoegfcffe "Stylish for Chrome"
[3]: mailto:randomglitch@gmail.com "Email Decompyler"
